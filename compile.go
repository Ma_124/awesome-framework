package awesomefw

import (
	"bufio"
	"github.com/schollz/progressbar/v2"
	"io"
	"os"
	"strings"
)

// The file format according to which the input will be parsed.
type Input interface {
	// This function should parse a line and call either out.Item() or out.Literal()
	ParseLine(w io.Writer, l string, out Output, ctx *Ctx)
}

// This is the context that is passed between Input, Output and FetchMeta.
type Ctx struct {
	fetchMetaGhRequests int

	// This is the API config passed to FetchMeta
	ApisCfg *ApisCfg
}

// An Item usually consists at least of a name, url and description
type Item struct {
	Name    string
	Url     string
	RepoUrl string
	Desc    string
}

// The output file format
type Output interface {
	// This should write the literal l to out
	Literal(w io.Writer, l string, ctx *Ctx)

	// This should write the formatted item to out
	Item(w io.Writer, item *Item, ctx *Ctx)

	// This function is called to initialize the Output
	Start(w io.Writer, ctx *Ctx)

	// This function is called to teardown the Output
	End(w io.Writer, ctx *Ctx)
}

// This holds ApiCfgs for each API used by FetchMeta
type ApisCfg struct {
	UseProgressBar bool
	GitHubCfg      *ApiCfg
	GitLabCfg      *ApiCfg
}

// This is used by FetchMeta to get the access token and the maximum number of requests for any given API.
type ApiCfg struct {
	// This is an access token passed to the API
	// NOTE: Make sure your access tokens aren't in the source code.
	AccessToken string

	// This variable specifies how many requests should be made to the API before falling back to a default response.
	// NOTE: This should only be used for development purposes.
	MaxRequests int
}

// Opens the file and runs Compile
func CompileFile(inpPath string, w io.Writer, inFF Input, outFF Output, apis *ApisCfg) {
	fi, err := os.Stat(inpPath)
	if err != nil {
		panic(err)
	}

	f, err := os.Open(inpPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	Compile(f, int(fi.Size()), w, inFF, outFF, apis)
}

func NewCtx(apis *ApisCfg) *Ctx {
	return &Ctx{
		ApisCfg: apis,
	}
}

func NewDebugReqsApisCfg() *ApisCfg {
	return &ApisCfg{
		GitHubCfg: &ApiCfg{"", 6000},
		GitLabCfg: &ApiCfg{"", 6000},
	}
}

func NewDebugCfg() *ApisCfg {
	return &ApisCfg{
		GitHubCfg: &ApiCfg{},
		GitLabCfg: &ApiCfg{},
	}
}

func NewDebugCtx() *Ctx {
	return NewCtx(NewDebugReqsApisCfg())
}

// Parses the file conforming to inFF and writes the output of outFF to out.
// The size argument is used to provide a progressbar.
// The apis config is forwarded to FetchMeta.
func Compile(in io.Reader, size int, w io.Writer, inFF Input, outFF Output, apis *ApisCfg) {
	var pb *progressbar.ProgressBar
	if apis.UseProgressBar {
		pb = progressbar.New(size)
	}

	r := bufio.NewReader(in)

	offset := 0

	ctx := NewCtx(apis)

	outFF.Start(w, ctx)

	eofReached := false
	for !eofReached {
		l, err := r.ReadString('\n')
		l = strings.TrimSuffix(l, "\r")
		if err != nil {
			if err == io.EOF {
				if len(l) == 0 {
					break
				}
				eofReached = true
				l += "\n"
			} else {
				panic(err)
			}
		}

		// TODO parallel

		inFF.ParseLine(w, l, outFF, ctx)

		offset += len(l)
		if pb != nil {
			_ = pb.Set(offset)
		}
	}

	outFF.End(w, ctx)
}
