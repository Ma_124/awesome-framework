package awesomefw

import (
	"io/ioutil"
	"strings"
	"testing"
)

func TestCompile_TestAlphaSorted(t *testing.T) {
	testTestOutput(t, "alpha_sorted", OutputTestAlphabetical(), false, `
A	Z	6
B	D	3
D	B	8
Z	A	0
`)
}

func TestCompile_TestAlphaUnsorted(t *testing.T) {
	testTestOutput(t, "alpha_unsorted", OutputTestAlphabetical(), true, `
A	Z	6
D	B	8
B	D	3
Z	A	0
`)
}

func TestCompile_TestAlphaDuplicate(t *testing.T) {
	testTestOutput(t, "alpha_dup", OutputTestAlphabetical(), true, `
A	Z	6
D	B	8
D	Y	5
Z	A	0
`)
}

func testTestOutput(t *testing.T, name string, out Output, fail bool, in string) {
	defer func() {
		r := recover()
		if (r == nil && !fail) || (r != nil && fail) {
			return
		}
		if fail {
			t.Fatal(name + ": Shoud have panicked!")
		} else {
			t.Fatal(name + ": Shoudn't have panicked!")
		}
	}()
	Compile(strings.NewReader(in), len(in), ioutil.Discard, InputTabSep(NameUrlDescTabSepPattern), out, NewDebugCfg())
}
