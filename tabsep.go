package awesomefw

import (
	"fmt"
	"io"
	"regexp"
	"strings"
)

var NameUrlDescTabSepPattern = []string{"name", "url", "desc"}
var tabsepRegexp = regexp.MustCompile(`(\s{3}|\t)\s*`)

type tabSepIO struct {
	Fields    []string
	inLiteral bool
}

// Parses the code according to this pattern:
//   <name>\t<url>\t<desc>
//   "literal line
//   'literal without newline
//   " continuation of previous line
//    """
//   block literal
//   which spans over
//   multiple lines
//   """
func InputTabSep(fields []string) Input {
	return &tabSepIO{fields, false}
}

func (io *tabSepIO) ParseLine(w io.Writer, l string, outFF Output, ctx *Ctx) {
	if strings.HasPrefix(l, `"""`) {
		// enter or leave literal block
		io.inLiteral = !io.inLiteral
		return
	}

	if io.inLiteral {
		outFF.Literal(w, l, ctx)
		return
	}

	if len(l) == 1 {
		return
	}

	if l[0] == '"' { // Start line literal
		outFF.Literal(w, l[1:], ctx)
		return
	}

	if l[0] == '\'' { // Start line literal without newline
		outFF.Literal(w, strings.TrimLeft(l[1:len(l)-1], "\r\n"), ctx)
		return
	}

	l = l[:len(l)-1]

	fields := tabsepRegexp.Split(l, -1)

	it := &Item{}
	i := 0

	for _, fieldDef := range io.Fields {
		if i >= len(fields) {
			panic(fmt.Sprintf("%q: expected <%s>\n", l, fieldDef))
		}

		switch fieldDef {
		case "name":
			it.Name = fields[i]
		case "url":
			it.Url = fields[i]
		case "repo-url":
			it.RepoUrl = fields[i]
		case "desc":
			it.Desc = fields[i]
		default:
			panic(fmt.Sprintf("%q: unknown fieldDef (%q)", l, fieldDef))
		}

		i++
	}

	outFF.Item(w, it, ctx)
}

// Prints the code according to this pattern:
//   <name>\t<url>\t<desc>
//   "literal line
//   'literal without newline
//   " continuation of previous line
//   "block literal
//   "which spans over
//   "multiple lines
func OutputTabSep(fields []string) Output {
	return &tabSepIO{fields, false}
}

func (tabSepIO) Start(w io.Writer, ctx *Ctx) {}
func (tabSepIO) End(w io.Writer, ctx *Ctx)   {}

func (tabSepIO) Literal(w io.Writer, l string, _ *Ctx) {
	/* TODO TabSep Out
	 * literal blocks
	 * align (once multitab support is added)
	 * multiple `'`s to `"`
	 */
	if l[len(l)-1] == '\n' {
		w.Write([]byte("\""))
		w.Write([]byte(l))
	} else {
		w.Write([]byte("'"))
		w.Write([]byte(l))
		w.Write([]byte("\n"))
	}
}

func (io tabSepIO) Item(w io.Writer, it *Item, _ *Ctx) {
	for i, fieldDef := range io.Fields {
		switch fieldDef {
		case "name":
			w.Write([]byte(it.Name))
		case "url":
			w.Write([]byte(it.Url))
		case "repo-url":
			w.Write([]byte(it.RepoUrl))
		case "desc":
			w.Write([]byte(it.Desc))
		default:
			panic(fmt.Sprintf("%v: unknown fieldDef (%q)", it, fieldDef))
		}

		if i < len(io.Fields)-1 {
			w.Write([]byte("\t"))
		}
	}

	w.Write([]byte("\n"))
}
