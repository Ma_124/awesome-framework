package awesomefw

import (
	"github.com/kballard/go-shellquote"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"strings"
	"testing"
)

func TestInputByName_Unknown(t *testing.T) {
	iff, err := InputByName("unknown_ ' - '")
	assert.Equal(t, "\"unknown_ ' - '\": unknown input name", err.Error())
	assert.Equal(t, (Input)(nil), iff)
}

func TestInputByName_MdListArgs(t *testing.T) {
	iff, err := InputByName("md:list ': '")
	assert.Equal(t, nil, err)

	buf := &strings.Builder{}
	iff.ParseLine(buf, "* [a](b): c\n", OutputTabSep(NameUrlDescTabSepPattern), NewDebugCtx())

	assert.Equal(t, "a\tb\tc\n", buf.String())
}

func TestInputByName_MdListDefArgs(t *testing.T) {
	iff, err := InputByName("md:list")
	assert.Equal(t, nil, err)

	buf := &strings.Builder{}
	iff.ParseLine(buf, "* [a](b) - c\n", OutputTabSep(NameUrlDescTabSepPattern), NewDebugCtx())

	assert.Equal(t, "a\tb\tc\n", buf.String())
}

func TestInputByName_MdListUnknownNArgs(t *testing.T) {
	iff, err := InputByName("md:list 'a' ''")
	assert.Equal(t, (Input)(nil), iff)
	assert.Equal(t, "\"md:list 'a' ''\": wrong number of arguments (usage: md [sep])", err.Error())
}

func TestInputByName_ShellquoteError(t *testing.T) {
	iff, err := InputByName("md:list '")
	assert.Equal(t, err.Error(), shellquote.UnterminatedSingleQuoteError.Error())
	assert.Equal(t, (Input)(nil), iff)
}

func TestInputByName_TabSepDefArgs(t *testing.T) {
	iff, err := InputByName("tabsep")
	assert.Equal(t, nil, err)

	buf := &strings.Builder{}
	iff.ParseLine(buf, "a\tb\tc\n", OutputTabSep(NameUrlDescTabSepPattern), NewDebugCtx())

	assert.Equal(t, "a\tb\tc\n", buf.String())
}

func TestOutputByName_MdTable(t *testing.T) {
	off, err := OutputByName("md:table")
	assert.Equal(t, nil, err)

	buf := &strings.Builder{}
	off.Item(buf, &Item{
		"a",
		"b",
		"",
		"c",
	}, NewDebugCtx())

	assert.Equal(t, "\n| Name | Desc |\n|------|------|\n| [a](b) | c |\n", buf.String())
}

func TestOutputByName_MdTableUnknownNArgs(t *testing.T) {
	off, err := OutputByName("md:table 'a'")
	assert.Equal(t, "\"md:table 'a'\": wrong number of arguments (usage: md:table)", err.Error())
	assert.Equal(t, (Output)(nil), off)
}

func TestOutputByName_MdTableMeta(t *testing.T) {
	off, err := OutputByName("md:table+meta")
	assert.Equal(t, nil, err)

	buf := &strings.Builder{}
	off.Item(buf, &Item{
		"a",
		"b",
		"",
		"c",
	}, NewDebugCtx())

	assert.Equal(t, "\n| Stars | Forks | Issues | Last Commit | Name | Desc |\n|-------|-------|--------|-------------|------|------|\n| N/A | N/A | N/A | N/A | [a](b) | c |\n", buf.String())
}

func TestOutputByName_MdTableMetaUnknownNArgs(t *testing.T) {
	off, err := OutputByName("md:table+meta 'a'")
	assert.Equal(t, "\"md:table+meta 'a'\": wrong number of arguments (usage: md:table+meta)", err.Error())
	assert.Equal(t, (Output)(nil), off)
}

func TestOutputByName_Html(t *testing.T) {
	off, err := OutputByName("html:table+meta")
	assert.Equal(t, nil, err)

	buf := &strings.Builder{}
	off.Item(buf, &Item{
		"a",
		"b",
		"",
		"c",
	}, NewDebugCtx())

	assert.Equal(t, "<table><thead><tr><th>Stars</th><th>Forks</th><th>Issues</th><th>Last Commit</th><th>Name</th><th>Desc</th></tr></thead><tbody>\n<tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td><a href=\"b\">a</a></td><td>c</td></tr>\n", buf.String())
}

func TestOutputByName_HtmlNArgs(t *testing.T) {
	off, err := OutputByName("html:table+meta 'a'")
	assert.Equal(t, "\"html:table+meta 'a'\": wrong number of arguments (usage: html)", err.Error())
	assert.Equal(t, (Output)(nil), off)
}

func TestOutputByName_ShellquoteError(t *testing.T) {
	off, err := OutputByName("md:table '")
	assert.Equal(t, err.Error(), shellquote.UnterminatedSingleQuoteError.Error())
	assert.Equal(t, (Output)(nil), off)
}

func TestOutputByName_Unknown(t *testing.T) {
	off, err := OutputByName("unknown_ ' - '")
	assert.Equal(t, "\"unknown_ ' - '\": unknown output name", err.Error())
	assert.Equal(t, (Output)(nil), off)
}

func TestIorOFFByName_ProcessEscEscArgs(t *testing.T) {
	iff, err := InputByName("md:list '\\\\'")
	assert.Equal(t, nil, err)

	buf := &strings.Builder{}
	iff.ParseLine(buf, "* [a](b)\\c\n", OutputTabSep(NameUrlDescTabSepPattern), NewDebugCtx())

	assert.Equal(t, "a\tb\tc\n", buf.String())
}

func TestInputByName_ProcessEscAtArgs(t *testing.T) {
	iff, err := InputByName("md:list '\\@'")
	assert.Equal(t, nil, err)

	buf := &strings.Builder{}
	iff.ParseLine(buf, "* [a](b)@c\n", OutputTabSep(NameUrlDescTabSepPattern), NewDebugCtx())

	assert.Equal(t, "a\tb\tc\n", buf.String())
}

func TestInputByName_ProcessAtArgs(t *testing.T) {
	f, err := ioutil.TempFile("", "awesome-framework-test-process-at-args-*.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	_, _ = f.Write([]byte("||"))

	iff, err := InputByName("md:list '@" + f.Name() + "'")
	assert.Equal(t, nil, err)

	buf := &strings.Builder{}
	iff.ParseLine(buf, "* [a](b)||c\n", OutputTabSep(NameUrlDescTabSepPattern), NewDebugCtx())

	assert.Equal(t, "a\tb\tc\n", buf.String())
}

func TestInputByName_ProcessAtArgsUnknownFile(t *testing.T) {
	iff, err := InputByName("md:list '@/unknown'")
	assert.Equal(t, (Input)(nil), iff)
	assert.Equal(t, `"md:list '@/unknown'": cannot open file "/unknown"`, err.Error())
}
