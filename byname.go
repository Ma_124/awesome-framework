package awesomefw

import (
	"fmt"
	"github.com/kballard/go-shellquote"
	"io/ioutil"
	"strings"
)

func InputByName(name string) (Input, error) {
	args, err := shellquote.Split(strings.TrimSpace(name))
	if err != nil {
		return nil, err
	}

	if err := processArgs(name, args); err != nil {
		return nil, err
	}

	switch args[0] {
	case "md:list":
		if len(args) == 1 {
			return InputMarkdown(" - "), nil
		} else if len(args) == 2 {
			return InputMarkdown(args[1]), nil
		} else {
			return nil, fmt.Errorf("%q: wrong number of arguments (usage: md [sep])", name)
		}
	case "tabsep":
		if len(args) == 1 {
			return InputTabSep(NameUrlDescTabSepPattern), nil
		} else {
			return InputTabSep(args[1:]), nil
		}
	default:
		return nil, fmt.Errorf("%q: unknown input name", name)
	}
}

func OutputByName(name string) (Output, error) {
	args, err := shellquote.Split(strings.TrimSpace(name))
	if err != nil {
		return nil, err
	}

	if err := processArgs(name, args); err != nil {
		return nil, err
	}

	switch args[0] {
	case "md:table":
		if len(args) != 1 {
			return nil, fmt.Errorf("%q: wrong number of arguments (usage: md:table)", name)
		}
		return OutputSimpleMarkdown(), nil
	case "md:table+meta":
		if len(args) != 1 {
			return nil, fmt.Errorf("%q: wrong number of arguments (usage: md:table+meta)", name)
		}
		return OutputMarkdown(), nil
	case "md:table>html":
		if len(args) != 1 {
			return nil, fmt.Errorf("%q: wrong number of arguments (usage: md:table)", name)
		}
		return OutputSimpleMarkdownHtml(), nil
	case "md:table+meta>html":
		if len(args) != 1 {
			return nil, fmt.Errorf("%q: wrong number of arguments (usage: md:table+meta)", name)
		}
		return OutputMarkdownHtml(), nil
	case "tabsep":
		if len(args) == 1 {
			return OutputTabSep(NameUrlDescTabSepPattern), nil
		} else {
			return OutputTabSep(args[1:]), nil
		}
	case "html:table+meta":
		if len(args) != 1 {
			return nil, fmt.Errorf("%q: wrong number of arguments (usage: html)", name)
		}
		return OutputHtml(), nil
	case "test:alpha":
		if len(args) != 1 {
			return nil, fmt.Errorf("%q: wrong number of arguments (usage: test:alpha)", name)
		}
		return OutputTestAlphabetical(), nil
	default:
		return nil, fmt.Errorf("%q: unknown output name", name)
	}
}

func processArgs(name string, args []string) error {
	for i := range args {
		if len(args[i]) > 1 && (args[i][0] == '\\' || args[i][1] == '\\') {
			args[i] = args[i][1:]
		} else if len(args[i]) > 1 && (args[i][0] == '\\' || args[i][1] == '@') {
			args[i] = args[i][1:]
		} else if len(args[i]) > 0 && (args[i][0] == '@') {
			s, err := ioutil.ReadFile(args[i][1:])
			if err != nil {
				return fmt.Errorf("%q: cannot open file %q", name, args[i][1:])
			}

			args[i] = string(s)
		}
	}

	return nil
}
