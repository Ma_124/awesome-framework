[![Build Status](https://gitlab.com/Ma_124/awesome-framework/badges/master/build.svg)](https://gitlab.com/Ma_124/awesome-framework/commits/master)
[![CodeCov](https://codecov.io/gl/Ma_124/awesome-framework/branch/master/graph/badge.svg)](https://codecov.io/gl/Ma_124/awesome-framework)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/Ma_124/awesome-framework)](https://goreportcard.com/report/gitlab.com/Ma_124/awesome-framework)
[![CII Best Practices](https://img.shields.io/cii/level/2506.svg)](https://bestpractices.coreinfrastructure.org/en/projects/2506)
[![Godoc](https://godoc.org/gitlab.com/Ma_124/awesome-framework?status.svg)](https://godoc.org/gitlab.com/Ma_124/awesome-framework)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://ma124.js.org/l/MIT/~/2019)

# Awesome Framework

<img src="https://gitlab.com/Ma_124/pages-static-shared/raw/master/awesome-scaffolding.png" width=300>

This is a framework for creating [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome/blob/master/awesome.md) lists. See it in action at a [fork of awesome-go](https://ma_124.gitlab.io/awesome-go-with-framework/)

It can convert [different filetypes](#filetypes) into each other and [fetch metadata](#fetch-metadata) about the repositories mentioned.

## `awesome.yml`

`awesome.yml`:
```yaml
#!/usr/bin/env awesome compile --config

lists:
- input: "inplist.md"
  format: "md:list"
  outputs:
  - file:   "outlist.md"
    format: "md:table+meta"
  - file:   "index.html"
    format: "html:table+meta"
  - file:   "/dev/stdout"
    format: "test-alpha"
```

`post-build.awesome.sh`:
```bash
#!/bin/bash

mkdir -p public
cp -r tmpl/** public

cat <<-EOF > public/index.html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        $(cat public/index.html)
    </body>
</html>
EOF
```

## Filetypes

### `md:list` (Input)

```md
+ lots
+ of
+ markdown
* [<name>](<url>) - <desc>
* [<name>](<url>) - <desc>

## Another Category
```

TODO: describe parameters

### `tabsep` (Input/Output)

```html
"literal line
'literal without newline
" continuation of previous line
<name>\t<url>\t<desc>
"""
block literal
which spans over
multiple lines
"""
```

TODO: describe parameters

### `md:table` (+ `>html`) (Output)

```md
| Name            | Desc   |
|-----------------|--------|
| [<name>](<url>) | <desc> |
```

If you append `>html` the markdown will be compiled with [Blackfriday][].

### `md:table+meta` (+ `>html`) (Output)

```md
| Stars   | Forks   | Issues   | Last Commit   | Name            | Desc   |
|---------|---------|----------|---------------|-----------------|--------|
| <stars> | <forks> | <issues> | <last commit> | [<name>](<url>) | <desc> |
```

If you append `>html` the markdown will be compiled with [Blackfriday][].

### `html:table+meta` (Output)

```html
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<main>
<table><thead><tr><th>Stars</th><th>Forks</th><th>Issues</th><th>Last Commit</th><th>Name</th><th>Desc</th></tr></thead><tbody>
<tr><td>&lt;Stars&gt;</td><td>&lt;Forks&gt;</td><td>&lt;Issues&gt;</td><td>&lt;Last Commit&gt;</td><td><a href="&lt;url&gt;">&lt;Name&gt;</a></td><td>&lt;Desc&gt;</td></tr>
</tbody></table>
		</main>
	</body>
</html>
```


### `test:alpha` (Test)

Tests whether the items are in alphabetically order.

## Fetch Metadata

Data Sources:
* https://github.com/example/repo
* http://github.com/example/repo
* https://godoc.org/github.com/example/repo
* http://godoc.org/github.com/example/repo
* https://gitlab.com/example/repo
* http://gitlab.com/example/repo
* https://godoc.org/gitlab.com/example/repo
* http://godoc.org/gitlab.com/example/repo

Fetched Metadata:
* Stars
* Forks
* Issues
* Last Commit

[Blackfriday]: https://github.com/russross/blackfriday

