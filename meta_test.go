package awesomefw

import (
	"context"
	"crypto/tls"
	"github.com/stretchr/testify/assert"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"
)

type testDefaultClientHandler struct {
	GitHub    http.Handler
	GitHubAPI http.Handler
	GitLab    http.Handler
}

const ghApiRepoResp = `{ "stargazers_count": 1234, "forks_count": 56, "open_issues_count": 7, "pushed_at": "2006-01-02T15:04:05Z" }`
const glApiRepoResp = `{ "star_count": 8901, "forks_count": 23, "last_activity_at": "2020-07-13T13:12:17Z" }`

func (h testDefaultClientHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	switch req.Host {
	case "github.com":
		h.GitHub.ServeHTTP(resp, req)
	case "api.github.com":
		h.GitHubAPI.ServeHTTP(resp, req)
	case "gitlab.com":
		h.GitLab.ServeHTTP(resp, req)
	}
}

func TestMain(m *testing.M) {
	oldHttpClient := *http.DefaultClient

	s := httptest.NewUnstartedServer(newTestDefaultClientHandler())
	s.StartTLS()

	defer func() {
		http.DefaultClient = &oldHttpClient
	}()
	defer s.Close()

	http.DefaultClient = &http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, network, _ string) (net.Conn, error) {
				return net.Dial(network, s.Listener.Addr().String())
			},
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	m.Run()
}

func newTestDefaultClientHandler() http.Handler {
	ghApi := http.NewServeMux()
	gh := http.NewServeMux()
	gl := http.NewServeMux()

	ghApi.HandleFunc("/repos/example/repo", func(resp http.ResponseWriter, req *http.Request) {
		resp.WriteHeader(200)
		resp.Write([]byte(ghApiRepoResp))
	})
	ghApi.HandleFunc("/repos/example/invjson", func(resp http.ResponseWriter, req *http.Request) {
		resp.WriteHeader(200)
		resp.Write([]byte(`{ "message": "`))
	})
	ghApi.HandleFunc("/repos/example/unknown", func(resp http.ResponseWriter, req *http.Request) {
		resp.WriteHeader(404)
		resp.Write([]byte(`{ "message": "unknown repo" }`))
	})
	gl.HandleFunc("/api/v4/projects/example/repo", func(resp http.ResponseWriter, req *http.Request) {
		resp.WriteHeader(200)
		resp.Write([]byte(glApiRepoResp))
	})
	gl.HandleFunc("/api/v4/projects/example/unknown", func(resp http.ResponseWriter, req *http.Request) {
		resp.WriteHeader(404)
		resp.Write([]byte(`{ "message": "unknown repo" }`))
	})

	return testDefaultClientHandler{
		GitHub:    gh,
		GitHubAPI: ghApi,
		GitLab:    gl,
	}
}

func TestFetchMeta(t *testing.T) {
	tests := []struct {
		URL      string
		Provider string
	}{
		{"https://github.com/example/repo/", "gh"},
		{"https://github.com/example/repo", "gh"},
		{"http://github.com/example/repo", "gh"},

		{"https://godoc.org/github.com/example/repo", "gh"},
		{"http://godoc.org/github.com/example/repo", "gh"},
		{"https://pkg.go.dev/github.com/example/repo", "gh"},
		{"http://pkg.go.dev/github.com/example/repo", "gh"},
		{"https://godoc.org/github.com/example/repo/", "gh"},
		{"http://godoc.org/github.com/example/repo//", "gh"},
		{"https://pkg.go.dev/github.com/example/repo//", "gh"},
		{"http://pkg.go.dev/github.com/example/repo/", "gh"},

		{"https://example.github.io/repo", "gh"},
		{"http://example.github.io/repo", "gh"},
		{"https://example.github.io/repo/", "gh"},
		{"http://example.github.io/repo/", "gh"},

		{"https://gitlab.com/example/repo", "gl"},
		{"http://gitlab.com/example/repo", "gl"},

		{"https://example.gitlab.io/repo", "gl"},
		{"http://example.gitlab.io/repo", "gl"},
		{"https://example.gitlab.io/repo/", "gl"},
		{"http://example.gitlab.io/repo/", "gl"},

		// example/unknown results in a 404 in the test round tripper
		{"http://github.com/example/unknown", "na"},
		{"https://example.com", "na"},
		{"http://example.com", "na"},
	}

	for i, test := range tests {
		t.Run(test.Provider+"_"+test.URL, func(t *testing.T) {
			m := FetchMeta(test.URL, &Ctx{ApisCfg: &ApisCfg{
				GitHubCfg: &ApiCfg{"GHTOK", 6000},
				GitLabCfg: &ApiCfg{"GLTOK", 6000},
			}})
			if test.Provider == "gh" {
				if m.Stars() != "1234" || m.Forks() != "56" || m.Issues() != "7" || m.LastCommit() != "2006-01-02" {
					t.Fatalf("%v: Unexpected output for GitHub API Response. (%T%+v)\n", i, m, m)
				}
			} else if test.Provider == "gl" {
				if m.Stars() != "8901" || m.Forks() != "23" || m.Issues() != NA || m.LastCommit() != "2020-07-13" {
					t.Fatalf("%v: Unexpected output for GitLab API Response. (%T%+v)\n", i, m, m)
				}
			} else if test.Provider == "na" {
				if m.Stars() != NA || m.Forks() != NA || m.Issues() != NA || m.LastCommit() != NA {
					t.Fatalf("%v: Unexpected output for N/A API Response. (%T%+v)\n", i, m, m)
				}
			}
		})
	}
}

func TestFetchMeta_InvalidJson(t *testing.T) {
	defer func() {
		r := recover()
		if _, ok := r.(error); ok {
			assert.Equal(t, "unexpected end of JSON input", r.(error).Error())
		} else {
			t.Fatal("panic error value has wrong type")
		}
	}()

	FetchMeta("https://github.com/example/invjson", NewDebugCtx())
}
