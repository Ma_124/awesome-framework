package awesomefw

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"strconv"
)

type cfg2 struct {
	Lists []*listCfg2 `yaml:"lists"`
}

type listCfg2 struct {
	InputPath   string        `yaml:"input"`
	InputFormat string        `yaml:"format"`
	Outputs     []*outputCfg2 `yaml:"outputs"`
}

type outputCfg2 struct {
	FilePath   string `yaml:"file"`
	FileFormat string `yaml:"format"`
}

func GetApisFromEnv(prefix string) (*ApisCfg, error) {
	ghMaxReqsS := os.Getenv(prefix + "MAX_GITHUB_REQS")

	var ghMaxReqs int
	var err error

	if ghMaxReqsS != "" {
		ghMaxReqs, err = strconv.Atoi(ghMaxReqsS)
		if err != nil {
			return nil, err
		}
	} else {
		ghMaxReqs = 6000
	}

	return &ApisCfg{
		false,
		&ApiCfg{
			os.Getenv(prefix + "GITHUB_TOKEN"),
			ghMaxReqs,
		},
		&ApiCfg{}, // TODO
	}, nil
}

func ExecuteConfigFile(f string, apis *ApisCfg) []error {
	data, err := ioutil.ReadFile(f)
	if err != nil {
		return []error{err}
	}

	return ExecuteConfig(data, apis)
}

func ExecuteConfig(data []byte, apis *ApisCfg) []error {
	cfg := &cfg2{}

	err := yaml.Unmarshal(data, cfg)
	if err != nil {
		return []error{err}
	}

	var errs []error

	for i, l := range cfg.Lists {
		for j, o := range l.Outputs {
			err = func(i, j int, l *listCfg2, o *outputCfg2) error {
				inp, err := InputByName(l.InputFormat)
				if err != nil {
					return err
				}

				out, err := OutputByName(o.FileFormat)
				if err != nil {
					return err
				}

				f, err := os.Create(o.FilePath)
				if err != nil {
					return err
				}
				defer f.Close()

				CompileFile(l.InputPath, f, inp, out, apis)

				return nil
			}(i, j, l, o)

			if err != nil {
				errs = append(errs, err)
			}
		}
	}

	if len(errs) == 0 {
		return nil
	}

	return errs
}
