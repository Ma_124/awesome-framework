package awesomefw

import "io"

func OutputHtml() Output {
	return &htmlOutput{true, false}
}

type htmlOutput struct {
	writeTableHead bool
	prevWasItem    bool
}

func (htmlOutput) Start(w io.Writer, ctx *Ctx) {}
func (htmlOutput) End(w io.Writer, ctx *Ctx)   {}

func (out *htmlOutput) Literal(w io.Writer, l string, ctx *Ctx) {
	if out.prevWasItem {
		out.prevWasItem = false
		w.Write([]byte(`</tbody></table>` + "\n"))
	}

	out.writeTableHead = true
	w.Write([]byte(l))
}

func (out *htmlOutput) Item(w io.Writer, item *Item, ctx *Ctx) {
	if out.writeTableHead {
		out.writeTableHead = false
		w.Write([]byte("<table><thead><tr><th>Stars</th><th>Forks</th><th>Issues</th><th>Last Commit</th><th>Name</th><th>Desc</th></tr></thead><tbody>\n"))
	}

	meta := FetchMeta(item.Url, ctx)

	w.Write([]byte("<tr><td>"))
	w.Write([]byte(meta.Stars()))
	w.Write([]byte("</td><td>"))
	w.Write([]byte(meta.Forks()))
	w.Write([]byte("</td><td>"))
	w.Write([]byte(meta.Issues()))
	w.Write([]byte("</td><td>"))
	w.Write([]byte(meta.LastCommit()))
	w.Write([]byte("</td><td><a href=\""))
	w.Write([]byte(item.Url))
	w.Write([]byte("\">"))
	w.Write([]byte(item.Name))
	w.Write([]byte("</a></td><td>"))
	w.Write([]byte(item.Desc))
	w.Write([]byte("</td></tr>\n"))

	out.prevWasItem = true
}
