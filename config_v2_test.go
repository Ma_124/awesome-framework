package awesomefw

import (
	"github.com/stretchr/testify/assert"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"
)

func TestExecuteConfigFile(t *testing.T) {
	in1 := writeTmpFile("in1", `
"# Awesome Awesome
"
"A curated list of awesome curated lists of many topics.
"
"- [Awesome Awesome](#awesome-awesome)
"    - [Computer management](#computer-management)
"## Computer management
"
awesome-scalability	https://github.com/binhnguyennus/awesome-scalability	An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems.
awesome-shell	https://github.com/alebcay/awesome-shell	Command-line frameworks, toolkits, guides and gizmos.
awesome-sysadmin	https://github.com/kahun/awesome-sysadmin	Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more.
"
"For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:])

	out11 := tmpFile("out11")
	out12 := tmpFile("out12")

	in2 := writeTmpFile("in2", `
# Awesome Awesome

A curated list of awesome curated lists of many topics.

- [Awesome Awesome](#awesome-awesome)
    - [Computer management](#computer-management)
## Computer management

* [awesome-scalability](https://github.com/binhnguyennus/awesome-scalability) - An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems.
* [awesome-shell](https://github.com/alebcay/awesome-shell) - Command-line frameworks, toolkits, guides and gizmos.
* [awesome-sysadmin](https://github.com/kahun/awesome-sysadmin) - Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more.

For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:])

	out21 := tmpFile("out21")

	cfg := writeTmpFile("cfg", `
#!/usr/bin/env awesome compile -config
lists:
  - input: "`+in1+`"
    format: "tabsep"
    outputs:
    - file: "`+out11+`"
      format: "test:alpha"
    - file: "`+out12+`"
      format: "md:table"
  - input: "`+in2+`"
    format: "md:list"
    outputs:
    - file: "`+out21+`""
      format: "tabsep"

# vim: set ts=2 sts=2 sw=2 noet:

`[1:])

	expectedOut11 := ``
	expectedOut12 := `
# Awesome Awesome

A curated list of awesome curated lists of many topics.

- [Awesome Awesome](#awesome-awesome)
    - [Computer management](#computer-management)
## Computer management


| Name | Desc |
|------|------|
| [awesome-scalability](https://github.com/binhnguyennus/awesome-scalability) | An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems. |
| [awesome-shell](https://github.com/alebcay/awesome-shell) | Command-line frameworks, toolkits, guides and gizmos. |
| [awesome-sysadmin](https://github.com/kahun/awesome-sysadmin) | Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more. |

For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:]

	expectedOut21 := `
"# Awesome Awesome
"
"A curated list of awesome curated lists of many topics.
"
"- [Awesome Awesome](#awesome-awesome)
"    - [Computer management](#computer-management)
"## Computer management
"
awesome-scalability	https://github.com/binhnguyennus/awesome-scalability	An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems.
awesome-shell	https://github.com/alebcay/awesome-shell	Command-line frameworks, toolkits, guides and gizmos.
awesome-sysadmin	https://github.com/kahun/awesome-sysadmin	Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more.
"
"For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:]

	errs := ExecuteConfigFile(cfg, NewDebugReqsApisCfg())
	if errs != nil {
		for _, err := range errs {
			t.Log("* " + err.Error() + "\n")
		}
		panic(errs[0])
	}

	EqualFileContent(t, out11, expectedOut11)
	EqualFileContent(t, out12, expectedOut12)
	EqualFileContent(t, out21, expectedOut21)
}

func TestGetApisFromEnvNormal(t *testing.T) {
	const prefix = "AWESOME_TEST_NORMAL_"
	err := os.Setenv(prefix+"MAX_GITHUB_REQS", "4000")
	if err != nil {
		panic(err)
	}

	err = os.Setenv(prefix+"GITHUB_TOKEN", "S3CR3T")
	if err != nil {
		panic(err)
	}

	apis, err := GetApisFromEnv(prefix)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, apis.GitHubCfg, &ApiCfg{
		"S3CR3T",
		4000,
	})
}

func TestGetApisFromEnvNoMaxReqs(t *testing.T) {
	const prefix = "AWESOME_TEST_NOMAXREQS_"
	err := os.Setenv(prefix+"MAX_GITHUB_REQS", "")
	if err != nil {
		panic(err)
	}

	err = os.Setenv(prefix+"GITHUB_TOKEN", "S3CR3T")
	if err != nil {
		panic(err)
	}

	apis, err := GetApisFromEnv(prefix)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, apis.GitHubCfg, &ApiCfg{
		"S3CR3T",
		6000,
	})
}

func TestGetApisFromEnvInvMaxReqs(t *testing.T) {
	const prefix = "AWESOME_TEST_INVMAXREQS_"
	err := os.Setenv(prefix+"MAX_GITHUB_REQS", "asdasds")
	if err != nil {
		panic(err)
	}

	err = os.Setenv(prefix+"GITHUB_TOKEN", "S3CR3T")
	if err != nil {
		panic(err)
	}

	_, err = GetApisFromEnv(prefix)
	assert.EqualError(t, err, `strconv.Atoi: parsing "asdasds": invalid syntax`)
}

func writeTmpFile(prefix string, content string) string {
	path := tmpFile(prefix)

	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	_, _ = f.Write([]byte(content))

	return path
}

var randSource = rand.NewSource(time.Now().Unix())

func tmpFile(prefix string) string {
	return filepath.Join(os.TempDir(), "awesome-framework-test-exe-cfg-"+prefix+"-"+strconv.FormatInt(randSource.Int63(), 36))
}
