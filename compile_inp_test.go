package awesomefw

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"
)

var awesomeAwesomeShortOut = `
"# Awesome Awesome
"
"A curated list of awesome curated lists of many topics.
"
"- [Awesome Awesome](#awesome-awesome)
"    - [Computer management](#computer-management)
"## Computer management
"
awesome-scalability	https://github.com/binhnguyennus/awesome-scalability	An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems.
awesome-shell	https://github.com/alebcay/awesome-shell	Command-line frameworks, toolkits, guides and gizmos.
awesome-sysadmin	https://github.com/kahun/awesome-sysadmin	Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more.
"
"For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:]

func TestCompile_TabSepTabSepInvalid(t *testing.T) {
	defer func() {
		r := recover()

		assert.Equal(t, "\"ghi\": expected <url>\n", r)
	}()

	in := `
"Abc
d	e	f
ghi
`[1:]

	Compile(strings.NewReader(in), len(in), &strings.Builder{}, InputTabSep(NameUrlDescTabSepPattern), OutputTabSep(NameUrlDescTabSepPattern), NewDebugCfg())
}

func TestCompile_MarkdownTabSep(t *testing.T) {
	out := &strings.Builder{}
	in := `
# Awesome Awesome

A curated list of awesome curated lists of many topics.

- [Awesome Awesome](#awesome-awesome)
    - [Computer management](#computer-management)
## Computer management

* [awesome-scalability](https://github.com/binhnguyennus/awesome-scalability) - An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems.
* [awesome-shell](https://github.com/alebcay/awesome-shell) - Command-line frameworks, toolkits, guides and gizmos.
* [awesome-sysadmin](https://github.com/kahun/awesome-sysadmin) - Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more.

For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:]
	Compile(strings.NewReader(in), len(in), out, InputMarkdown(" - "), OutputTabSep(NameUrlDescTabSepPattern), NewDebugCfg())
	NewDebugReqsApisCfg()

	assert.Equal(t, awesomeAwesomeShortOut, out.String())
}

func TestCompileFile_TmpFile(t *testing.T) {
	f, err := ioutil.TempFile("", "awesome-framework-test-compile-file-*.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	_, _ = f.Write([]byte(awesomeAwesomeShortOut[:len(awesomeAwesomeShortOut)-1]))

	_ = f.Close()

	buf := &strings.Builder{}
	CompileFile(f.Name(), buf, InputTabSep(NameUrlDescTabSepPattern), OutputTabSep(NameUrlDescTabSepPattern), NewDebugCfg())

	assert.Equal(t, buf.String(), awesomeAwesomeShortOut)
}

func TestCompileFile_UnknownFile(t *testing.T) {
	defer func() {
		r := recover()
		if _, ok := r.(error); ok {
			assert.Equal(t, r.(error).Error(), "stat /unknown: no such file or directory")
		} else {
			t.Fail()
		}
	}()

	CompileFile("/unknown", nil, InputTabSep(NameUrlDescTabSepPattern), OutputTabSep(NameUrlDescTabSepPattern), nil)
}

func TestCompileFile_NoReadPermFile(t *testing.T) {
	fname := filepath.Join(os.TempDir(), "awesome-framework-test-no-read-perm-"+strconv.FormatInt(randSource.Int63(), 36)+".txt")

	defer func() {
		r := recover()
		if _, ok := r.(error); ok {
			assert.Equal(t, r.(error).Error(), "open "+fname+": permission denied")
		} else {
			t.Fail()
		}
	}()

	_, err := os.OpenFile(fname, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0200)
	if err != nil {
		panic(err)
	}

	CompileFile(fname, &strings.Builder{}, InputTabSep(NameUrlDescTabSepPattern), OutputTabSep(NameUrlDescTabSepPattern), NewDebugCfg())
}

type testCompileFileReadErr struct{}

func (testCompileFileReadErr) Read(p []byte) (n int, err error) {
	return 0, errors.New("error by mocked reader")
}

func TestCompileFile_ReadErr(t *testing.T) {
	defer func() {
		r := recover()

		assert.Equal(t, r, errors.New("error by mocked reader"))
	}()

	Compile(&testCompileFileReadErr{}, 0, &strings.Builder{}, InputTabSep(NameUrlDescTabSepPattern), OutputTabSep(NameUrlDescTabSepPattern), NewDebugCfg())
}
