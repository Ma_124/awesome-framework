package awesomefw

import (
	"io"
)

/* TODO https://gitlab.com/Ma_124/awesome-go/blob/master/repo_test.go
 * format?
 */

type noopOutput struct{}

func (noopOutput) Literal(w io.Writer, l string, ctx *Ctx) {}
func (noopOutput) Item(w io.Writer, item *Item, ctx *Ctx)  {}
func (noopOutput) Start(w io.Writer, ctx *Ctx)             {}
func (noopOutput) End(w io.Writer, ctx *Ctx)               {}

type alphaOutput struct {
	noopOutput
	Last string
}

// Panics if the items aren't alphabetically sorted. Nothing is written to out
func OutputTestAlphabetical() Output {
	return &alphaOutput{}
}

func (out *alphaOutput) Item(w io.Writer, item *Item, ctx *Ctx) {
	if out.Last >= item.Name {
		panic("not in alphabetical order or a duplicate")
	}
	out.Last = item.Name
}

// TODO check URL scheme
// TODO wrapper OFF(...OFF)
