package awesomefw

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Check that naMeta and ghMeta implement Meta
var _ Meta = naMeta{}
var _ Meta = ghMeta{}
var _ Meta = glMeta{}

// Metadata about a repository
type Meta interface {
	// The number of stars or N/A
	Stars() string

	// The number of forks or N/A
	Forks() string

	// The number of issues or N/A
	Issues() string

	// The date of the last commit or N/A
	LastCommit() string

	// File returns the URL under which the given path may be found or "".
	// There must not be any checks whether the file actually exists.
	File(path string) string
}

// Just N/A
const NA = "N/A"

type naMeta struct{}

func (naMeta) Stars() string {
	return NA
}

func (naMeta) Forks() string {
	return NA
}

func (naMeta) Issues() string {
	return NA
}

func (naMeta) LastCommit() string {
	return NA
}

func (naMeta) File(path string) string {
	return ""
}

var httpPattern = regexp.MustCompile(`^(https?)?://`)

// Fetches a Meta object from the APIs
// For supported URL schemas see the tests in meta_test.go/TestFetchMeta()
func FetchMeta(url string, ctx *Ctx) Meta {
	// strip scheme
	if is := httpPattern.FindStringIndex(url); is != nil {
		url = url[is[1]:]
	}

	if strings.HasPrefix(url, "godoc.org") {
		url = url[9:]
	}

	if strings.HasPrefix(url, "pkg.go.dev") {
		url = url[10:]
	}

	url = strings.Trim(url, "/")
	ps := strings.SplitN(url, "/", 2)
	ps[0] = strings.ToLower(ps[0])

	var m Meta

	switch ps[0] {
	case "github.com":
		m = fetchGitHub(ps[1], ctx)
	case "gitlab.com":
		m = fetchGitLab(ps[1], ctx)
	case "gopkg.in":
		// TODO implement
		return &naMeta{}
	default:
		if strings.HasSuffix(ps[0], ".github.io") {
			m = fetchGitHub(ps[0][:len(ps[0])-10]+"/"+ps[1], ctx)
		} else if strings.HasSuffix(ps[0], ".gitlab.io") {
			m = fetchGitLab(ps[0][:len(ps[0])-10]+"/"+ps[1], ctx)
		} else {
			m = &naMeta{}
		}
	}

	// TODO wrap in Meta which provides Badges (e.g. GoDoc/pkg.go.dev/module support)

	return m
}

type ghMeta struct {
	RepoVar       string `json:"-"`
	StarsVar      int    `json:"stargazers_count"`
	ForksVar      int    `json:"forks_count"`
	IssuesVar     int    `json:"open_issues_count"`
	LastCommitVar string `json:"pushed_at"`
	MessageVar    string `json:"message"`
}

func (m ghMeta) Stars() string {
	return strconv.Itoa(m.StarsVar)
}

func (m ghMeta) Forks() string {
	return strconv.Itoa(m.ForksVar)
}

func (m ghMeta) Issues() string {
	return strconv.Itoa(m.IssuesVar)
}

func (m ghMeta) LastCommit() string {
	return m.LastCommitVar
}

func (m ghMeta) File(path string) string {
	return "https://raw.githubusercontent.com/" + m.RepoVar + "/master/" + path
}

func fetchGitHub(repo string, ctx *Ctx) Meta {
	var data []byte
	if ctx.fetchMetaGhRequests < ctx.ApisCfg.GitHubCfg.MaxRequests {
		ctx.fetchMetaGhRequests++
		resp, err := http.Get("https://api.github.com/repos/" + repo + "?access_token=" + ctx.ApisCfg.GitHubCfg.AccessToken)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		data, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		_ = resp.Body.Close()

		remainingS := resp.Header.Get("X-RateLimit-Remaining")
		if remainingS != "" {
			remaining, _ := strconv.Atoi(remainingS)
			if remaining <= 1 {
				reset, err := strconv.Atoi(resp.Header.Get("X-RateLimit-Reset"))
				if err != nil {
					panic(err)
				}

				t := time.Unix(int64(reset), 0)
				sleep := t.Sub(time.Now())
				fmt.Printf("Sleep for %v...\n", sleep)
				if sleep < 5 * time.Minute {
					time.Sleep(sleep)
				} else {
					panic("GitHub: RateLimit: try again at " + t.In(time.Local).Format("2006-01-02 15:04:05 MST"))
				}
			}
		}
	} else {
		// TODO only in test
		data = []byte(`{ "stargazers_count": 7654, "forks_count": 32, "open_issues_count": 1, "pushed_at": "2019-01-20T19:24:24Z" }`)
	}

	jsO := &ghMeta{}
	err := json.Unmarshal(data, jsO)
	if err != nil {
		panic(err)
	}

	if jsO.MessageVar != "" {
		return &naMeta{}
	}

	lastC, err := time.Parse(time.RFC3339, jsO.LastCommitVar)
	if err == nil {
		jsO.LastCommitVar = lastC.Format("2006-01-02")
	}

	return jsO
}

type glMeta struct {
	RepoVar       string `json:"repo_var"`
	StarsVar      int    `json:"star_count"`
	ForksVar      int    `json:"forks_count"`
	LastCommitVar string `json:"last_activity_at"`
	MessageVar    string `json:"message"`
}

func (m glMeta) Stars() string {
	return strconv.Itoa(m.StarsVar)
}

func (m glMeta) Forks() string {
	return strconv.Itoa(m.ForksVar)
}

func (m glMeta) Issues() string {
	// TODO
	return NA
}

func (m glMeta) LastCommit() string {
	return m.LastCommitVar
}

func (m glMeta) File(path string) string {
	return "https://gitlab.com/" + m.RepoVar + "/-/raw/master/" + path
}

func fetchGitLab(repo string, ctx *Ctx) Meta {
	resp, err := http.Get("https://gitlab.com/api/v4/projects/" + repo + "?access_token=" + ctx.ApisCfg.GitLabCfg.AccessToken)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	_ = resp.Body.Close()

	jsO := &glMeta{}
	err = json.Unmarshal(data, jsO)
	if err != nil {
		panic(err)
	}

	if jsO.MessageVar != "" {
		return &naMeta{}
	}

	lastC, err := time.Parse(time.RFC3339, jsO.LastCommitVar)
	if err == nil {
		jsO.LastCommitVar = lastC.Format("2006-01-02")
	}

	return jsO
}
