package awesomefw

import (
	"github.com/russross/blackfriday/v2"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

var awesomeAwesomeShortInp = `
'# Awesome
" Awesome
"""

A curated list of awesome curated lists of many topics.

- [Awesome Awesome](#awesome-awesome)
    - [Computer management](#computer-management)
"""

"## Computer management
"
awesome-scalability    https://github.com/binhnguyennus/awesome-scalability    An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems.
awesome-shell          https://github.com/alebcay/awesome-shell	               Command-line frameworks, toolkits, guides and gizmos.
awesome-sysadmin       https://github.com/kahun/awesome-sysadmin               Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more.
"
"For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:]

func TestCompile_TabSepTabSep(t *testing.T) {
	out := &strings.Builder{}
	Compile(strings.NewReader(awesomeAwesomeShortInp), len(awesomeAwesomeShortInp), out, InputTabSep(NameUrlDescTabSepPattern), OutputTabSep(NameUrlDescTabSepPattern), NewDebugCfg())

	assert.Equal(t, `
'# Awesome
" Awesome
"
"A curated list of awesome curated lists of many topics.
"
"- [Awesome Awesome](#awesome-awesome)
"    - [Computer management](#computer-management)
"## Computer management
"
awesome-scalability	https://github.com/binhnguyennus/awesome-scalability	An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems.
awesome-shell	https://github.com/alebcay/awesome-shell	Command-line frameworks, toolkits, guides and gizmos.
awesome-sysadmin	https://github.com/kahun/awesome-sysadmin	Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more.
"
"For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:], out.String())
}

func TestCompile_TabSepSimpleMarkdown(t *testing.T) {
	out := &strings.Builder{}
	Compile(strings.NewReader(awesomeAwesomeShortInp), len(awesomeAwesomeShortInp), out, InputTabSep(NameUrlDescTabSepPattern), OutputSimpleMarkdown(), NewDebugCfg())

	assert.Equal(t, `
# Awesome Awesome

A curated list of awesome curated lists of many topics.

- [Awesome Awesome](#awesome-awesome)
    - [Computer management](#computer-management)
## Computer management


| Name | Desc |
|------|------|
| [awesome-scalability](https://github.com/binhnguyennus/awesome-scalability) | An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems. |
| [awesome-shell](https://github.com/alebcay/awesome-shell) | Command-line frameworks, toolkits, guides and gizmos. |
| [awesome-sysadmin](https://github.com/kahun/awesome-sysadmin) | Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more. |

For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:], out.String())
}

func TestCompile_TabSepMarkdown(t *testing.T) {
	out := &strings.Builder{}
	Compile(strings.NewReader(awesomeAwesomeShortInp), len(awesomeAwesomeShortInp), out, InputTabSep(NameUrlDescTabSepPattern), OutputMarkdown(), NewDebugCfg())

	assert.Equal(t, `
# Awesome Awesome

A curated list of awesome curated lists of many topics.

- [Awesome Awesome](#awesome-awesome)
    - [Computer management](#computer-management)
## Computer management


| Stars | Forks | Issues | Last Commit | Name | Desc |
|-------|-------|--------|-------------|------|------|
| 7654 | 32 | 1 | 2019-01-20 | [awesome-scalability](https://github.com/binhnguyennus/awesome-scalability) | An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems. |
| 7654 | 32 | 1 | 2019-01-20 | [awesome-shell](https://github.com/alebcay/awesome-shell) | Command-line frameworks, toolkits, guides and gizmos. |
| 7654 | 32 | 1 | 2019-01-20 | [awesome-sysadmin](https://github.com/kahun/awesome-sysadmin) | Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more. |

For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:], out.String())
}

func TestCompile_TabSepSimpleMarkdownHtml(t *testing.T) {
	out := &strings.Builder{}
	Compile(strings.NewReader(awesomeAwesomeShortInp), len(awesomeAwesomeShortInp), out, InputTabSep(NameUrlDescTabSepPattern), OutputSimpleMarkdownHtml(), NewDebugCfg())

	assert.Equal(t, string(blackfriday.Run([]byte(`
# Awesome Awesome

A curated list of awesome curated lists of many topics.

- [Awesome Awesome](#awesome-awesome)
    - [Computer management](#computer-management)
## Computer management


| Name | Desc |
|------|------|
| [awesome-scalability](https://github.com/binhnguyennus/awesome-scalability) | An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems. |
| [awesome-shell](https://github.com/alebcay/awesome-shell) | Command-line frameworks, toolkits, guides and gizmos. |
| [awesome-sysadmin](https://github.com/kahun/awesome-sysadmin) | Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more. |

For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:]), blackfridayExtensions)), out.String())
}

func TestCompile_TabSepMarkdownHtml(t *testing.T) {
	out := &strings.Builder{}
	Compile(strings.NewReader(awesomeAwesomeShortInp), len(awesomeAwesomeShortInp), out, InputTabSep(NameUrlDescTabSepPattern), OutputMarkdownHtml(), NewDebugCfg())

	assert.Equal(t, string(blackfriday.Run([]byte(`
# Awesome Awesome

A curated list of awesome curated lists of many topics.

- [Awesome Awesome](#awesome-awesome)
    - [Computer management](#computer-management)
## Computer management


| Stars | Forks | Issues | Last Commit | Name | Desc |
|-------|-------|--------|-------------|------|------|
| 7654 | 32 | 1 | 2019-01-20 | [awesome-scalability](https://github.com/binhnguyennus/awesome-scalability) | An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems. |
| 7654 | 32 | 1 | 2019-01-20 | [awesome-shell](https://github.com/alebcay/awesome-shell) | Command-line frameworks, toolkits, guides and gizmos. |
| 7654 | 32 | 1 | 2019-01-20 | [awesome-sysadmin](https://github.com/kahun/awesome-sysadmin) | Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more. |

For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
`[1:]), blackfridayExtensions)), out.String())
}

func TestCompile_TabSepHtml(t *testing.T) {
	out := &strings.Builder{}
	in := `
"<main>
a	b	c
d	e	f
"<h2>Section 2</h2>
g	h	i
j	k	l
"</main>`[1:]

	Compile(strings.NewReader(in), len(in), out, InputTabSep(NameUrlDescTabSepPattern), OutputHtml(), NewDebugCfg())

	assert.Equal(t, `
<main>
<table><thead><tr><th>Stars</th><th>Forks</th><th>Issues</th><th>Last Commit</th><th>Name</th><th>Desc</th></tr></thead><tbody>
<tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td><a href="b">a</a></td><td>c</td></tr>
<tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td><a href="e">d</a></td><td>f</td></tr>
</tbody></table>
<h2>Section 2</h2>
<table><thead><tr><th>Stars</th><th>Forks</th><th>Issues</th><th>Last Commit</th><th>Name</th><th>Desc</th></tr></thead><tbody>
<tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td><a href="h">g</a></td><td>i</td></tr>
<tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td><a href="k">j</a></td><td>l</td></tr>
</tbody></table>
</main>
`[1:], out.String())
}
