# Awesome Awesome

A curated list of awesome curated lists of many topics.

- [Awesome Awesome](#awesome-awesome)
    - [Computer management](#computer-management)


## Computer management

* [awesome-scalability](https://github.com/binhnguyennus/awesome-scalability) - An up-to-date and curated reading list for designing high scalability, high availability, high stabilityback-end systems.
* [awesome-shell](https://github.com/alebcay/awesome-shell) - Command-line frameworks, toolkits, guides and gizmos.
* [awesome-sysadmin](https://github.com/kahun/awesome-sysadmin) - Backups, configuration management, DNS, IMAP/POP3, LDAP, monitoring, SSH, statistics, troubleshooting, virtualization, VPN and more.

For contributing, [open an issue](https://github.com/emijrp/awesome-awesome/issues) and/or a [pull request](https://github.com/emijrp/awesome-awesome/pulls). Above there are some ideas for missing lists, be bold!
