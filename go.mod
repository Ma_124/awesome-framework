module gitlab.com/Ma_124/awesome-framework

go 1.14

require (
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/russross/blackfriday/v2 v2.0.1
	github.com/schollz/progressbar/v2 v2.15.0
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/stretchr/testify v1.6.1
	gopkg.in/yaml.v2 v2.3.0
)
