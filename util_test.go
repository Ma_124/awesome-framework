package awesomefw

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
)

// EqualFileContent checks whether a file has the expected content.
func EqualFileContent(t assert.TestingT, path string, content string, msgAndArgs ...interface{}) bool {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return assert.Fail(t, fmt.Sprintf("error when running ioutil.ReadFile(%q): %s", path, err), msgAndArgs...)
	}

	return assert.Equal(t, content, string(data), msgAndArgs)
}
