package awesomefw

import (
	"bytes"
	"github.com/russross/blackfriday/v2"
	"io"
	"regexp"
	"strings"
)

// Parses the code according to this pattern:
//   * [<name>](<url>) - <desc>
//   - [<name>](<url>) - <desc>
//
// sep: " - "
func InputMarkdown(sep string) Input {
	return markdownInput{sep}
}

var pattern = regexp.MustCompile(`^\s*[-*] \[.*?\]`)

type markdownInput struct {
	Sep string
}

func (inp markdownInput) ParseLine(w io.Writer, l string, outFF Output, ctx *Ctx) {
	starIndx := strings.IndexByte(l, '*')
	dashIndx := strings.IndexByte(l, '-')
	if starIndx < 0 || (dashIndx >= 0 && dashIndx < starIndx) {
		starIndx = dashIndx
	}
	if pattern.MatchString(l) {
		afterNameI := strings.IndexByte(l, ']')
		outFF.Item(w,
			&Item{
				l[starIndx+3 : afterNameI],
				strings.TrimSpace(l[afterNameI+2 : strings.IndexByte(l[afterNameI+2:], ')')+afterNameI+2]),
				"",
				strings.TrimSpace(l[strings.Index(l, inp.Sep)+len(inp.Sep) : len(l)-1]),
			},
			ctx)
	} else {
		outFF.Literal(w, l, ctx)
	}
}

type simpleMarkdownOutput struct {
	writeTableHead bool
}
type markdownOutput struct{ simpleMarkdownOutput }

// Prints the code according to this pattern:
//
// | Name            | Desc   |
// |-----------------|--------|
// | [<name>](<url>) | <desc> |
func OutputSimpleMarkdown() Output {
	return &simpleMarkdownOutput{true}
}

// Prints the code according to this pattern:
//
// | Stars   | Forks   | Issues   | Last Commit   | Name            | Desc   |
// |---------|---------|----------|---------------|-----------------|--------|
// | <stars> | <forks> | <issues> | <last commit> | [<name>](<url>) | <desc> |
func OutputMarkdown() Output {
	return &markdownOutput{simpleMarkdownOutput{true}}
}

func (simpleMarkdownOutput) Start(w io.Writer, ctx *Ctx) {}
func (simpleMarkdownOutput) End(w io.Writer, ctx *Ctx)   {}

func (out *simpleMarkdownOutput) Literal(w io.Writer, l string, ctx *Ctx) {
	out.writeTableHead = true
	w.Write([]byte(l))
}

func (out *simpleMarkdownOutput) Item(w io.Writer, item *Item, ctx *Ctx) {
	if out.writeTableHead {
		out.writeTableHead = false
		w.Write([]byte("\n| Name | Desc |\n"))
		w.Write([]byte("|------|------|\n"))
	}

	w.Write([]byte("| ["))
	w.Write([]byte(item.Name))
	w.Write([]byte("]("))
	w.Write([]byte(item.Url))
	w.Write([]byte(") | "))
	w.Write([]byte(item.Desc))
	w.Write([]byte(" |\n"))
}

func (out *markdownOutput) Item(w io.Writer, item *Item, ctx *Ctx) {
	if out.writeTableHead {
		out.writeTableHead = false
		w.Write([]byte("\n| Stars | Forks | Issues | Last Commit | Name | Desc |\n"))
		w.Write([]byte("|-------|-------|--------|-------------|------|------|\n"))
	}

	meta := FetchMeta(item.Url, ctx)

	w.Write([]byte("| "))
	w.Write([]byte(meta.Stars()))
	w.Write([]byte(" | "))
	w.Write([]byte(meta.Forks()))
	w.Write([]byte(" | "))
	w.Write([]byte(meta.Issues()))
	w.Write([]byte(" | "))
	w.Write([]byte(meta.LastCommit()))
	w.Write([]byte(" | ["))
	w.Write([]byte(item.Name))
	w.Write([]byte("]("))
	w.Write([]byte(item.Url))
	w.Write([]byte(") | "))
	w.Write([]byte(item.Desc))
	w.Write([]byte(" |\n"))
}

// Same as OutputSimpleMarkdown but compiled to HTML by github.com/russross/blackfriday/v2
func OutputSimpleMarkdownHtml() Output {
	return OutputBlackfriday(OutputSimpleMarkdown(), blackfridayExtensions)
}

// Same as OutputMarkdown but compiled to HTML by github.com/russross/blackfriday/v2
func OutputMarkdownHtml() Output {
	return OutputBlackfriday(OutputMarkdown(), blackfridayExtensions)
}

var blackfridayExtensions = blackfriday.WithExtensions(blackfriday.CommonExtensions | blackfriday.AutoHeadingIDs)

type blackfridayOutputWrapper struct {
	Previous Output
	Options  []blackfriday.Option
	buf      *bytes.Buffer
}

func (out blackfridayOutputWrapper) Literal(w io.Writer, l string, ctx *Ctx) {
	out.Previous.Literal(out.buf, l, ctx)
}

func (out blackfridayOutputWrapper) Item(w io.Writer, item *Item, ctx *Ctx) {
	out.Previous.Item(out.buf, item, ctx)
}

func (out blackfridayOutputWrapper) Start(w io.Writer, ctx *Ctx) {
	out.Previous.Start(out.buf, ctx)
}

func (out blackfridayOutputWrapper) End(w io.Writer, ctx *Ctx) {
	out.Previous.End(out.buf, ctx)
	w.Write(blackfriday.Run(out.buf.Bytes(), out.Options...))
}

func OutputBlackfriday(prev Output, opts ...blackfriday.Option) Output {
	return blackfridayOutputWrapper{
		Previous: prev,
		Options:  opts,
		buf:      &bytes.Buffer{},
	}
}
